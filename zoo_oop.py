class Zoo:
    def __init__(self, adresa):
        self.zvirata = []
        self.adresa = adresa

    def print2():
        print('{}'.format(adresa))

    def test(self):
        l = []
        for i in range(len(self.zvirata)):
            if self.zvirata[i].healthy == False:
                print ('False')
            elif self.zvirata[i].healthy == True:
                print ('True')

        print('Byla vylecena zvirata', l)

    def print(self):
        y = len(self.zvirata)
        prubeh = int(0)
        print('Celkem je v seznamu', y, 'zvirat')
        for i in range(len(self.zvirata)):
            prubeh = prubeh + 1
            if self.zvirata[i].sex == 'male':
                if self.zvirata[i].healthy == True:
                    if self.zvirata[i].fed == True:
                        print('Zvire', prubeh, 'je', self.zvirata[i].species, 'se jmenem', self.zvirata[i].name,
                              '. Je to samec, je zdravy a nema hlad')
                    else:
                        print('Zvire', prubeh, 'je', self.zvirata[i].species, 'se jmenem', self.zvirata[i].name,
                              '. Je to samec, je zdravy a ma hlad')
                else:
                    if self.zvirata[i].fed == True:
                        print('Zvire', prubeh, 'je', self.zvirata[i].species, 'se jmenem', self.zvirata[i].name,
                              '. Je to samec, neni zdravy a nema hlad')
                    else:
                        print('Zvire', prubeh, 'je', self.zvirata[i].species, 'se jmenem', self.zvirata[i].name,
                              '. Je to samec, neni zdravy a ma hlad')
            else:
                if self.zvirata[i].healthy == True:
                    if self.zvirata[i].fed == True:
                        print('Zvire', prubeh, 'je', self.zvirata[i].species, 'se jmenem', self.zvirata[i].name,
                              '. Je to samice, je zdrava a nema hlad')
                    else:
                        print('Zvire', prubeh, 'je', self.zvirata[i].species, 'se jmenem', self.zvirata[i].name,
                              '. Je to samice, je zdrava a ma hlad')
                else:
                    if self.zvirata[i].fed == True:
                        print('Zvire', prubeh, 'je', self.zvirata[i].species, 'se jmenem', self.zvirata[i].name,
                              '. Je to samice, neni zdrava a nema hlad')
                    else:
                        print('Zvire', prubeh, 'je', self.zvirata[i].species, 'se jmenem', self.zvirata[i].name,
                              '. Je to samice, neni zdrava a ma hlad')

    def pridej(self):
        species = input('Zadejte druh: \n')
        name = input('Zadejte jméno: \n')
        sex = input('Zadejte pohlaví: \n')
        fedz = input('Zadejte zda je zvíře nakrmeno: \n')
        if fedz == "True":
            fed = True
        else: fed = False
        healthyz = input('Zadejte zda je zvíře zdravé: \n')
        if healthyz == "True":
            healthy = True
        else: healthy = False
        obj = Animal(species, name, sex, fed, healthy)
        self.zvirata.append(obj)

    def pridej_zaklad(self, species, name, sex, fed, healthy):
        obj = Animal(species, name, sex, fed, healthy)
        self.zvirata.append(obj)

    def change_fed(self):
        l = []
        for i in range(len(self.zvirata)):
            if self.zvirata[i].fed == False:
                l.append(self.zvirata[i].name)
                self.zvirata[i].fed = True
        print('Byla nakrmena zvirata', l)

    def change_health(self):
        l = []
        for i in range(len(self.zvirata)):
            if self.zvirata[i].healthy == False:
                l.append(self.zvirata[i].name)
                self.zvirata[i].healthy = True
        print('Byla vylecena zvirata', l)

    def krmeni(self):
        nakr = 0
        for i in range(len(self.zvirata)):
            if self.zvirata[i].fed == False:
                print('Zvire', self.zvirata[i].name, 'ma hlad')
                nakr = nakr + 1
        print('Je hladovych', nakr, 'zvirat')

    def zdravi(self):
        uzdr = 0
        for i in range(len(self.zvirata)):
            if self.zvirata[i].healthy == False:
                print('Zvire', self.zvirata[i].name, 'je nemocne')
                uzdr = uzdr + 1
        print('Je nemocnych', uzdr, 'zvirat')

class Animal:
    def __init__(self, species, name, sex, fed, healthy):
        self.species = species
        self.name = name
        self.sex = sex
        self.fed = fed
        self.healthy = healthy

    def __str__(self):
        return ('Zvire druhu {} se jmenem {} nema hlad {} a je zdrave {}'.format(self.species, self.name, self.sex, self.fed, self.healthy))

class Adress:
    def __init__(self, street, code, city, zipcode):
        self.street = street
        self.code = code
        self.city = city
        self.zipcode = zipcode

    def __str__(self):
        return ('ZOO lezi na adrese {}, {} {}, {}'.format(self.city, self.street, self.code, self.zipcode))


adresa = Adress('Kotlarska', '12', 'Brno', '60200')
ZooBrno = Zoo(adresa)
ZooBrno.pridej_zaklad('Wolf', 'Boris', 'male', True, True)
ZooBrno.pridej_zaklad('Wolf', 'Sable', 'female', False, True)
ZooBrno.pridej_zaklad('Elephant', 'Suki', 'female', True, False)
ZooBrno.pridej_zaklad('Eagle', 'Comet', 'male', False, False)

NAME_OF_THE_ZOO = 'Gotham'
print('Vítejte v informačním systému ZOO {}'.format(NAME_OF_THE_ZOO))


while True:
    answer = int(input('Vyberte jednu z možností:\n'
                       '1: Přidat nové zvíře\n'
                       '2: Nakrmit zvířata\n'
                       '3: Výlečit zvířata\n'
                       '4: Najít zvířata co mají hlad\n'  # fed == False
                       '5: Nají zvířata co jsou nemocná\n'  # healthy == False
                       '6: Vypsat všechna zvířata\n'
                       '7: Vypsat adresu\n'
                       '8: Ukončit systém\n'))

    if answer == 1:
        ZooBrno.pridej()

    if answer == 6:
        ZooBrno.print()

    if answer == 2:
        ZooBrno.change_fed()

    if answer == 3:
        ZooBrno.change_health()

    if answer == 4:
        ZooBrno.krmeni()

    if answer == 5:
        ZooBrno.zdravi()

    if answer == 7:
        Zoo.print2()

    elif answer == 8:
        print('Ukončování programu...')
        break