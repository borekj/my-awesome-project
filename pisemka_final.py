def novy():
    x=0
    nazev1 = input('Zadejte název: \n')
    omezeni1 = int(input('Zadejte věkové omezení: \n'))
    delka1 = int(input('Zadejte délku: \n'))
    den1 = input('Zadejte datum vysílaní: \n')
    cas_zacatek1 = input('Zadej zadejte začátek vysílání: \n')
    platba = (input('Zadej, zdali je placen (ano/ne) \n'))

    for i in programy:
        if i['nazev'] == nazev1:
            if i['omezeni'] == omezeni1:
                if i['delka'] == delka1:
                    if i['cas_zacatek'] == cas_zacatek1:
                        print('Takový pořad už existuje')
                        x=1;
    if platba=='ano':
        placenost1 = True
    else: placenost1 = False

    delka2=delka1
    if x==0:
        z = 0
        c = cas_zacatek1.split(":")
        a = int(c[0])
        b = int(c[1])
        while delka1 > 60:
            delka1 = delka1 - 60
            z = z + 1
        a = a + z
        b = b + delka1
        if b >= 60:
            a = a + 1
            b = b - 60
        if a > 23:
            a = a - 24
        ck1 = "{}".format(a)
        ck2 = "{}".format(b)
        if len(ck1) == 1:
            ck1 = "0{}".format(a)
        if len(ck2) == 1:
            ck2 = "0{}".format(b)
        cas_konec1 = "{}:{}".format(ck1, ck2)
        programy.append({'nazev': nazev1, 'omezeni': omezeni1, 'delka': delka2, 'den': den1, 'cas_zacatek': cas_zacatek1, 'cas_konec': cas_konec1, 'placenost': placenost1},)

def vek(x):
    o=0
    l=[]
    for i in programy:
        if i['omezeni']<=x:
            l.append(i['nazev'])
            o=1
    if o==0:
        print('Nemůžete se dívat na žádný pořad')
    else: print('Muzes se divat na tyto porady:', *l, sep='\n')

def placenost(x):
    l = []
    if x==1:
        for i in programy:
            if i['placenost']==True:
                l.append(i['nazev'])
    if x==0:
        for i in programy:
            if i['placenost']==False:
                l.append(i['nazev'])
    print('Muzes se divat na tyto porady:', *l, sep='\n')

def datum(x):
    o=0
    l = []
    for i in programy:
        if i['den']==x:
            l.append(i['nazev'])
            o=1
    if o==0:
       print('Toto datum se nevysílají žádné pořady')
    else: print('Muzes se divat na tyto porady:', *l, sep='\n')

def vymazani(x):
    r=0
    for i in range(len(programy)):
        if programy[i]['nazev'] == x:
            del programy[i]
            r=1
            break
    if r==0:
        print('Takový pořad nebyl nalezen')
    else: print('Pořad byl vymazán')

def cas(x):
    c = x.split(":")
    x = 0
    l=[]
    for i in programy:
        p = i['cas_zacatek'].split(":")
        k = i['cas_konec'].split(":")
        if p[0] < c[0] < k[0]:
            l.append(i['nazev'])
            x = 1
        if p[0] == c[0]:
            if p[1] <= c[1]:
                l.append(i['nazev'])
                x = 1
        if k[0] == c[0]:
            if c[1] <= k[1]:
                l.append(i['nazev'])
                x = 1
    if x == 0:
        print('Nic se nevysila')
    else:
        print('Muzes se divat na tyto porady:', *l, sep='\n')


programy = [
    {'nazev': 'Moje zprávy', 'omezeni': 15, 'delka': 100, 'den': '12.11.2019', 'cas_zacatek': '20:00', 'cas_konec': '21:40', 'placenost': True},
    {'nazev': 'Soudkyně Barbara', 'omezeni': 18, 'delka': 150, 'den': '14.11.2019', 'cas_zacatek': '19:10', 'cas_konec': '21:40', 'placenost': True},
    {'nazev': 'Aréna Jaromira Soukupa', 'omezeni': 15, 'delka': 120, 'den': '15.11.2019', 'cas_zacatek': '21:00', 'cas_konec': '23:00', 'placenost': False},
    {'nazev': 'Týden s prezidentem', 'omezeni': 50, 'delka': 250, 'den': '12.11.2019', 'cas_zacatek': '17:50', 'cas_konec': '22:00', 'placenost': False}
]

import time

while True:
    time.sleep(1)
    answer = int(input('Vyberte jednu z možností:\n'
                       '1: Přidat pořad\n'
                       '2: Pořady podle věku\n'
                       '3: Podle data\n'
                       '4: Podle placenosti\n'     
                       '5: Vymazat pořad\n'   
                       '6: Podle času\n'
                       '7: Ukoncit program\n' ))


    if answer == 1:
        novy()

    if answer == 2:
        vek(int(input('Kolik Vám je let? \n')))

    if answer == 3:
        datum(input('Zadejte datum \n'))

    if answer == 4:
        placenost(int(input('Zadejte 1 pro placené a 0 pro neplacené pořady \n')))

    if answer == 5:
        vymazani(input('Zadejte název pořadu, který chcete vymazat \n'))

    if answer == 6:
        cas(input('Zadejte čas \n'))

    elif answer == 7:
        print('Ukončování programu...')
        break